<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('profile', 'ProfileController@index')->name('profile')->middleware('verified');
Route::post('invite-friend', 'ProfileController@inviteFriend')->name('invite')->middleware('verified');
Route::get('invitation-accepting/{id}', 'ProfileController@accept')->name('accept');
// Route::get('friends', 'ProfileController@index')->name('friends');
// Route::get('profile', 'ProfileController@index')->name('home')->middleware('verified');
