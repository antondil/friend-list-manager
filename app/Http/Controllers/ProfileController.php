<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Friendship;
use Auth;
use App\Mail\FriendRequest;
use App\Mail\newFriendLogin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
   {
        $friends = Friendship::where('user_id',auth()->user()->id)->get();
        return view('profile.profile',['friends'=>$friends]);
   }              
               
    // public function invite()
    // {
    //     return view('profile.invite_friend');
    // }

    public function inviteRequest(Request $request)
    {
        $data = request()->validate([
    		'friend_fname' => 'required',
            'friend_email' => 'required|email',
        ]);
        
        $friend = new Friend();
        $friend->friend_fname = $data['friend_name'];
        $friend->friend_email = $data['friend_email'];
        $friend->user_id = auth()->user()->id;
        $friend->status = 0;
        $friend->save();

        $data['url'] = url('invitation-accepting/'.$friend->id);
        $data['inviter_name'] = auth()->user()->firstname .' '.auth()->user()->lastname;
        $data['friend_name'] = $data['friend_name'];
       
        self::sendInvitationMail($data);
        return redirect('/profile')->with('success','Friend request sent successfully');
    }

    public function sendInvitationMail($data)
    {
        Mail::to($data['friend_email'])->send(new FriendRequest($data));        
    }

    public  function accept($token)
    {
        
        $friendship = Friendship::find($token);
        $friendship->status = 1;
        $friendship->save();
        
        return redirect('/');

    }

    
}
