<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Friendship;
use Auth;
use App\Mail\FriendRequest;
use App\Mail\newFriendLogin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class FriendController extends Controller
{
   public function index()
   {
        $friends = Friendship::where('user_id',auth()->user()->id)->get();
        return view('profile',['friends'=>$friends]);
   }              
               
    public function invite()
    {
        return view('invite');
    }

    public function sendRequest(Request $request)
    {
        $data = request()->validate([
    		'fname' => 'required',
            'f-email' => 'required|email',
        ]);
        
        $friend = new Friend();
        $friend->friend_fname = $data['fname'];
        $friend->friend_email = $data['f-email'];
        $friend->user_id = auth()->user()->id;
        $friend->accepted_friend_id = 0;
        $friend->status = 0;
        $friend->save();
        $data['url'] = url('approvedrequest/'.$friend->id);
        $data['username'] = auth()->user()->firstname .' '.auth()->user()->lastname;
       
        self::sendRequestToFriend($data);
        return redirect('/home')->with('success','Friend request send successfully');
    }

    public function sendRequestToFriend($data)
    {
        Mail::to($data['f-email'])->send(new FriendRequest($data));        
    }

    public  function acceptRequest($id)
    {
        
        $friend = Friend::find($id);
        $friend->status = 1;
        $friend->save();

        $count = self::checkFriendRegistration($friend->friend_email);
        
        if($count == 0)
        {
            $data = self::registerNewFriend($id);
            $friend = Friend::find($id);
            $friend->accepted_friend_id = $data['friend_id'];
            $friend->save();

            self::newFriendLogin($data);
            return redirect('/')->with('success','Your Login Details will be send soon');
        }
        return redirect('/');

    }

    public function newFriendLogin($data)
    {
        $user = User::find($data['friend_id']);
        Mail::to($user['email'])->send(new newFriendLogin($user));
    }

    public static function registerNewFriend($id)
    {
        $friend = Friend::find($id);
        
        $user = new User();
        $user->firstname = $friend->friend_fname;
        $user->email = $friend->friend_email;
        $user->password             = Hash::make('W;>9XqQ`rnZ,_25*');
        $user->email_verified_at    = date('Y-m-d H:i:s');
        $user->save();
        
        $data = [
            'friend_id' => $user->id,
            'user_id'   => $id
        ];
        return $data;
    }

    public static function checkFriendRegistration($data)
    {    
       $count = User::where('email',$data)->exists();
       return $count;
    }

    public function deleteFriend($id)
    {
        $friend = Friend::find($id);
        $friend->delete();
        return redirect('/home')->with('success','Removed successfuly');
    }

    public static function search(Request $request)
    {
        $friend = $request->friendname;
        $user_detail = Friend::Where('friend_fname', 'like', '%' . $friend . '%')->where('user_id',auth()->user()->id)->orWhere('accepted_friend_id',auth()->user()->id)->first();
        return view('show',['user_detail'=>$user_detail,]);
          
    }
}
