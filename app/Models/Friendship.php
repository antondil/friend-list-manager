<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    // protected $fillable = [
    //     'friend_email','friend_fname'
    // ];

    // protected $hidden = [
    //     'verify_token', 'status',
    // ];

    // protected $casts = [
    //     'accepted_at' => 'datetime',
    // ];

    // protected $primaryKey = 'friendship_id';

    public function inviter()
    {
        return $this->belongsTo('App\User');
    }
}
