@extends('layouts.app')

@section('content')
<div class="container">
    <button class="btn btn-primary" style="margin-bottom:20px" data-toggle="modal" data-target="#invite-friend">Invite Friend</button>

    <table class="table table-hover" id="friends-table">
    <thead>
      <tr>
        <!-- <th>Name</th> -->
        <th>Friend Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Remove Friend</th>
      </tr>
    </thead>
    <tbody>
        @foreach($friends as $friend)
            <tr>
                <!-- <td>{{ $friend->user->firstname }}</td> -->
                <td>{{ $friend->friend_fname }}</td>
                <td>{{$friend->friend_email}}</td>
                <td><?php echo $friend->status == 1 ? 'Approved' :'Pending'; ?></td>
                <td><a href ="/delete/{{ $friend->id}}" class="btn btn-danger">Delete</a></td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>

<script>
$(document).ready(function() {
    $('#friends-table').DataTable();
} );
</script>




<div class="modal fade" id="invite-friend" tabindex="-1" role="dialog" aria-labelledby="invite-friendLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{url('invite-friend')}}" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Invite a friend</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{csrf_field()}}
          <div class="form-group row">
            <label for="friend_name" class="col-sm-4 col-form-label">Friend Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="friend_name" name="friend_name">
            </div>
          </div>
          <div class="form-group row">
            <label for="friend_email" class="col-sm-4 col-form-label">Email</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="friend_email" name="friend_email">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Invite</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection








