@extends('layouts.app')

@section('content')
<div class="container">
<h3 >Invite Friend </h3>
<form action="/send-request" method="post">
{{csrf_field()}}
  <div class="form-group row">
    <label for="friend_name" class="col-sm-2 col-form-label">Friend Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="friend_name" name="friend_name">
    </div>
  </div>
  <div class="form-group row">
    <label for="friend_email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="friend_email" name="friend_email">
    </div>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
</form>
</div>
@endsection
